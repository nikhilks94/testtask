package com.nikhil.testtask.network;

import com.nikhil.testtask.model.Albums;
import com.nikhil.testtask.model.Images;
import com.nikhil.testtask.model.Users;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiCalls {

    @GET("/users/")
    Call<ArrayList<Users>> getUsers();

    @GET("/albums/")
    Call<ArrayList<Albums>> getAlbumbs(@Query("userId") String id);

    @GET("/photos/")
    Call<ArrayList<Images>> getPhotos(@Query("albumId") String id);

}
