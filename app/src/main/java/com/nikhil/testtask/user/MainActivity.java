package com.nikhil.testtask.user;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ListView;

import com.nikhil.testtask.R;
import com.nikhil.testtask.model.Users;
import com.nikhil.testtask.network.RetrofitApi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    ListView usersList;
    ProgressDialog progressDialog;
    UserAdapter userAdapter;
    Call<ArrayList<Users>> usersCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle("Users");

        usersList = findViewById(R.id.list_users);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading");
        userAdapter = new UserAdapter(MainActivity.this,new ArrayList<Users>());
        getUsers();

    }

    void getUsers(){
        progressDialog.show();
        usersCall = RetrofitApi.getService().getUsers();
        usersCall.enqueue(new Callback<ArrayList<Users>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<Users>> call, @NonNull Response<ArrayList<Users>> response) {
                progressDialog.cancel();
                progressDialog.dismiss();
                if(response.isSuccessful()){
                    userAdapter = new UserAdapter(MainActivity.this,response.body());
                    usersList.setAdapter(userAdapter);
                    userAdapter.notifyDataSetChanged();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("Something went wrong. PLease try again");
                    builder.setCancelable(false);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getUsers();
                        }
                    });
                    builder.show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<Users>> call,@NonNull Throwable t) {
                progressDialog.cancel();
                progressDialog.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Please check your internet connection and try again.");
                builder.setCancelable(false);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getUsers();
                    }
                });
                builder.show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        usersCall.cancel();
        if(progressDialog!=null && progressDialog.isShowing()){
            progressDialog.dismiss();
            progressDialog.cancel();
            progressDialog.dismiss();
        }
    }
}
