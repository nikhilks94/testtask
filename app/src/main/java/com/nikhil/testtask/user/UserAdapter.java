package com.nikhil.testtask.user;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.nikhil.testtask.R;
import com.nikhil.testtask.album.AlbumsActivity;
import com.nikhil.testtask.model.Users;

import java.util.ArrayList;
import java.util.List;


public class UserAdapter extends ArrayAdapter<Users> {

    private Context mContext;
    private List<Users> users = new ArrayList<>();

    public UserAdapter(Context context, ArrayList<Users> list) {
        super(context, 0 , list);
        mContext = context;
        users = list;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.item_user,parent,false);

        final Users user = users.get(position);

        TextView name = (TextView) listItem.findViewById(R.id.name);
        name.setText(user.getName());

        TextView release = (TextView) listItem.findViewById(R.id.email);
        release.setText(user.getEmail());

        listItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, AlbumsActivity.class);
                intent.putExtra("id",String.valueOf(user.getId()));
                mContext.startActivity(intent);
            }
        });

        return listItem;
    }
}