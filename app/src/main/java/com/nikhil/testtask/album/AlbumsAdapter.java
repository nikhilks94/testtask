package com.nikhil.testtask.album;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.nikhil.testtask.R;
import com.nikhil.testtask.model.Albums;
import com.nikhil.testtask.model.Users;
import com.nikhil.testtask.photos.PhotosActivity;

import java.util.ArrayList;
import java.util.List;

public class AlbumsAdapter extends ArrayAdapter<Albums> {

    private Context mContext;
    private List<Albums> users = new ArrayList<>();

    public AlbumsAdapter(Context context, ArrayList<Albums> list) {
        super(context, 0 , list);
        mContext = context;
        users = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.item_album,parent,false);

        final Albums albums = users.get(position);

        TextView albumName = (TextView) listItem.findViewById(R.id.album_name);
        albumName.setText(albums.getTitle());

        listItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, PhotosActivity.class);
                intent.putExtra("id",String.valueOf(albums.getId()));
                mContext.startActivity(intent);
            }
        });

        return listItem;
    }
}