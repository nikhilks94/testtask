package com.nikhil.testtask.album;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ListView;

import com.nikhil.testtask.R;
import com.nikhil.testtask.model.Albums;
import com.nikhil.testtask.network.RetrofitApi;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlbumsActivity extends AppCompatActivity {

    ListView albumList;
    ProgressDialog progressDialog;
    AlbumsAdapter albumsAdapter;
    Call<ArrayList<Albums>> albumsCall;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_albums);

        getSupportActionBar().setTitle("Albums");

        id = getIntent().getStringExtra("id");
        albumList = findViewById(R.id.list_albums);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading");
        albumsAdapter = new AlbumsAdapter(AlbumsActivity.this,new ArrayList<Albums>());
        getAlbums();
    }

    void getAlbums(){
        progressDialog.show();
        albumsCall = RetrofitApi.getService().getAlbumbs(id);
        albumsCall.enqueue(new Callback<ArrayList<Albums>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<Albums>> call, @NonNull Response<ArrayList<Albums>> response) {
                progressDialog.cancel();
                progressDialog.dismiss();
                if(response.isSuccessful()){
                    albumsAdapter = new AlbumsAdapter(AlbumsActivity.this,response.body());
                    albumList.setAdapter(albumsAdapter);
                    albumsAdapter.notifyDataSetChanged();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(AlbumsActivity.this);
                    builder.setMessage("Something went wrong. PLease try again");
                    builder.setCancelable(false);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getAlbums();
                        }
                    });
                    builder.show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<Albums>> call,@NonNull Throwable t) {
                progressDialog.cancel();
                progressDialog.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(AlbumsActivity.this);
                builder.setMessage("Please check your internet connection and try again.");
                builder.setCancelable(false);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getAlbums();
                    }
                });
                builder.show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        albumsCall.cancel();
        if(progressDialog!=null && progressDialog.isShowing()){
            progressDialog.dismiss();
            progressDialog.cancel();
            progressDialog.dismiss();
        }
    }
}
