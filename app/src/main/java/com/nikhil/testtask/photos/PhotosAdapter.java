package com.nikhil.testtask.photos;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nikhil.testtask.R;
import com.nikhil.testtask.model.Images;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.ViewHolder> {

    Context context;
    ArrayList<Images> imageList;

    public PhotosAdapter(Context context, ArrayList<Images> imageList) {
        this.context = context;
        this.imageList = imageList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_images,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso.get().load(imageList.get(position).getThumbnailUrl()).placeholder(R.drawable.square_placeholder).centerCrop().fit().into(holder.imageView);
    }

    public void setImageList(ArrayList<Images> imageList) {
        this.imageList = imageList;
    }

    @Override
    public int getItemCount() {
        if(imageList == null)
        return 0;
        return imageList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView  = itemView.findViewById(R.id.image);
        }
    }
}
