package com.nikhil.testtask.photos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import com.nikhil.testtask.R;
import com.nikhil.testtask.album.AlbumsActivity;
import com.nikhil.testtask.model.Images;
import com.nikhil.testtask.network.RetrofitApi;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhotosActivity extends AppCompatActivity {

    RecyclerView imageList;
    GridLayoutManager gridLayoutManager;
    PhotosAdapter photosAdapter;
    Call<ArrayList<Images>> call;
    String id;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos);

        getSupportActionBar().setTitle("Photos");

        id = getIntent().getStringExtra("id");
        imageList = findViewById(R.id.images_list);
        gridLayoutManager = new GridLayoutManager(this,2);
        imageList.setLayoutManager(gridLayoutManager);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading");
        photosAdapter = new PhotosAdapter(this,new ArrayList<Images>());
        imageList.setAdapter(photosAdapter);
        getPhotos();
    }

    private void getPhotos() {
        progressDialog.show();
        call = RetrofitApi.getService().getPhotos(id);
        call.enqueue(new Callback<ArrayList<Images>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<Images>> call, @NonNull Response<ArrayList<Images>> response) {
                progressDialog.cancel();
                progressDialog.dismiss();
                if(response.isSuccessful()){
                    ArrayList<Images> results = response.body();
                    photosAdapter.setImageList(results);
                    photosAdapter.notifyDataSetChanged();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(PhotosActivity.this);
                    builder.setMessage("Something went wrong. PLease try again");
                    builder.setCancelable(false);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getPhotos();
                        }
                    });
                    builder.show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<Images>> call, @NonNull Throwable t) {

                progressDialog.cancel();
                progressDialog.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(PhotosActivity.this);
                builder.setMessage("Please check your internet connection and try again.");
                builder.setCancelable(false);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getPhotos();
                    }
                });
                builder.show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        call.cancel();
        if(progressDialog!=null && progressDialog.isShowing()){
            progressDialog.dismiss();
            progressDialog.cancel();
            progressDialog.dismiss();
        }
    }
}
